---
sidebar_position: 4
---

# Создание запроса на слияние

После отправки изменений в удаленный репозитория в GitLab необходимо создать запрос на слияние (Merge Request).

Запрос создается на слияние ветки, в рамках которой выполнялось задание лабораторной работы, и веткой по умолчанию (`master` или `main`).

В запросе на слияние в качестве назначенного лица (`Assignee`) указывается преподаватель. Также необходимо выбрать пункты `Delete source branch when merge request is accepted` и `Squash commits when merge request is accepted`.

В видео демонстрируются выполнение данных действий, которые необходимы для создания запроса на слияние:

<iframe src="https://vk.com/video_ext.php?oid=-215612996&id=456239022&hash=0ccc8a078a921add&hd=2" width="100%" height="480" allow="autoplay; encrypted-media; fullscreen; picture-in-picture;" frameborder="0" allowfullscreen></iframe>
