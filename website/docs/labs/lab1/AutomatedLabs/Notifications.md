---
sidebar_position: 7
---

# Уведомления

Чтобы не пропустить события, которые происходят в GitLab, рекомендуются настроить уведомления. Для этого в разделе `Пользовательские настройки > Уведомления` для группы, в которой был создан репозиторий для решений, необходимо указать события, о которых будут приходить уведомления.

Шаг 1:

![Шаг 1](images/notifications-1.png)

Шаг 2:

![Шаг 2](images/notifications-2.png)

Шаг 3:

![Шаг 3](images/notifications-3.png)

Рекомендуется выбрать следующие уведомления:

- `New merge request`
- `Push to merge request`
- `Reopen merge request`
- `Close merge request`
- `Reassign merge request`
- `Change reviewer merge request`
- `Merge merge request`
- `Failed pipeline`
- `Fixed pipeline`
- `Successful pipeline`

Шаг 4:

![Шаг 4](images/notifications-4.png)
