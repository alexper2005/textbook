---
sidebar_position: 1
---

# Решение и отправка задания с помощью Visual Studio 2022 и CMake

В данной статье демонстрируется работа с репозиторием в Visual Studio 2022 и CMake.

Основные действия, которые необходимо предпринять:

1. Получить персональный токен доступа в GitLab и клонировать репозиторий;
2. Выбрать CMake файл для рабочего пространства;
3. Создать новую ветку, в рамках которой будет выполняться задание лабораторной работы;
4. Назначить исполняемый файл;
5. Редактировать файл с решением задания;
6. Исправить проблемы с кодировкой;
7. Отладить программу;
8. Добавить в игнорируемые служебные файлы;
9. Фиксация и отправка изменений в удаленный репозиторий.

В видео демонстрируются выполнение данных действий:

<iframe src="https://vk.com/video_ext.php?oid=-215612996&id=456239018&hash=f8a69b29c9c626f6&hd=2" width="100%" height="480" allow="autoplay; encrypted-media; fullscreen; picture-in-picture;" frameborder="0" allowfullscreen></iframe>
