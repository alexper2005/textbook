---
sidebar_position: 2
---

# Решение и отправка задания с помощью Visual Studio 2022 и проекта Console App (Windows)

В данной статье демонстрируется работа с репозиторием в Visual Studio 2022 и GitKraken.

Основные действия, которые необходимо предпринять:

1. Получить персональный токен доступа в GitLab и клонировать репозиторий;
2. Создать новую ветку, в рамках которой будет выполняться задание лабораторной работы;
3. Создать проект Console App (Windows);
4. Переместить проект в папку с заданием;
5. Добавить файл решения в проект;
6. Редактировать файл с решением задания;
7. Исправить проблемы с кодировкой;
8. Отладить программу;
9. Добавить в игнорируемые служебные файлы;
10. Фиксация и отправка изменений в удаленный репозиторий.

В видео демонстрируются выполнение данных действий:

<iframe src="https://vk.com/video_ext.php?oid=-215612996&id=456239020&hash=5b2305c1378613b5&hd=2" width="100%" height="480" allow="autoplay; encrypted-media; fullscreen; picture-in-picture;" frameborder="0" allowfullscreen></iframe>
