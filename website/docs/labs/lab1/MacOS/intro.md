# Разработка в операционной системе macOS

Для работы в Visual Studio Code в macOS рекомендуется прочитать данную [статью](https://code.visualstudio.com/docs/cpp/config-clang-mac).

Установка CLion представлена в [статье](https://www.jetbrains.com/help/clion/installation-guide.html).
