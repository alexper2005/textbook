---
sidebar_position: 3
---

# Разработка в семействе операционных систем в Linux

Для работы в Visual Studio Code в Linux рекомендуется прочитать данную [статью](https://code.visualstudio.com/docs/cpp/config-linux).

Установка CLion представлена в [статье](https://www.jetbrains.com/help/clion/installation-guide.html).
