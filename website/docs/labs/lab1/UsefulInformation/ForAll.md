---
sidebar_position: 1
---

# Для всех

## Ошибка `CMakeFiles/lab1.dir/main.c.obj` missing and no known to make it

При попытке собрать проект с помощь CMake возникает ошибка:

![error](images/error-cmakefiles.jpg)

Проблема заключается в присутствии кириллицы в пути проекта.

**Решение**: склонировать репозиторий в папку, где нет кириллицы.
