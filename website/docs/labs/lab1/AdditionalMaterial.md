---
sidebar_position: 9
---

# Дополнительный материал

## Оформление кода

1. [Руководство Google по стилю в C++](https://habr.com/ru/post/480422/) [(eng)](https://google.github.io/styleguide/cppguide.html)
2. [Гайд по оформлению кода на С++ от Стэнфордского университета](https://tproger.ru/translations/stanford-cpp-style-guide/)
3. [How to write Clean, Beautiful and Effective C++ Code](https://clightning.medium.com/how-to-write-clean-beautiful-and-effective-c-code-d4699f5e3864)

## О C

### Книги

1. И. С. Солдатенко. Основы программирования на языке Си
2. СиБрайан Керниган, Деннис Ритчи. Язык программирования

### Электронные ресурсы

1. [Видеокурс для изучения языка Си с нуля](https://tproger.ru/video/clang-for-beginners-videos/)
2. [Введение и справочное руководство по языку GNU C](https://w96k.dev/public/doc/gnu-c/)
3. [Введение в язык СИ](https://dfe.petrsu.ru/koi/posob/c/)

## О С++

### Книги

1. Орленко П. А., Евдокимов П. В. С++ на примерах. Практика, практика и только практика
