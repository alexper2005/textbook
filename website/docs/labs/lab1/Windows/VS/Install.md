---
sidebar_position: 1
---

# Установка Visual Studio

Visual Studio Community - бесплатная полнофункциональная расширяемая среда IDE для создания современных приложений Android, iOS и Windows, а также веб-приложений и облачных служб. Установщик можно скачать с [сайта](https://visualstudio.microsoft.com/ru/vs/community/).

Шаг 1:

![Установка VS  - шаг 1](images/vs-install-step1.png)

Шаг 2:

![Установка VS  - шаг 2](images/vs-install-step2.png)

Шаг 3:

![Установка VS  - шаг 3](images/vs-install-step3.png)

Шаг 4 (ожидание подготовки установщика):

![Установка VS  - шаг 4](images/vs-install-step4.png)

Перечень рабочих нагрузок:

- Разработка классических приложений на C++
- Linux и встроенная разработка на C++

Шаг 5 (выбор рабочей нагрузки):

![Установка VS  - шаг 5](images/vs-install-step5.png)

Шаг 6 (выбор рабочей нагрузки):

![Установка VS  - шаг 6](images/vs-install-step6.png)

Шаг 6 (ожидание установки рабочих нагрузок):

![Установка VS  - шаг 7](images/vs-install-step7.png)

После установки запустите Visual Studio.
