---
sidebar_position: 2
---

# Создание проекта и его отладка в Visual Studio

В приветственном окне Visual Studio выберем пункт создание нового проекта:

![Debug в VS - шаг 1](images/vs-debug-step1.png)

Необходимо выбрать консольное приложение на C++ для платформы Windows:

![Debug в VS - шаг 2](images/vs-debug-step2.png)

Указываем где разместить создаваемый проект:

![Debug в VS - шаг 3](images/vs-debug-step3.png)

Окно с новым проектом выглядит так:

![Debug в VS - шаг 4](images/vs-debug-step4.png)

Добавьте кириллические букв в вывод программы, чтобы убедиться, что набор инструментов настроен правильно. Код для проверки расположен [здесь](../../code/main.cpp).

Запустите на отладку проект:

![Debug в VS - шаг 5](images/vs-debug-step5.png)

В случае успеха в панели консоли будет виден вывод:

![Debug в VS - шаг 6](images/vs-debug-step6.png)
