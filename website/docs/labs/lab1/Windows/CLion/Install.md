---
sidebar_position: 1
---

# Установка CLion

CLion - интегрированная среда разработки для языков программирования Си и C++, разрабатываемая компанией JetBrains. Скачать можно [здесь](https://www.jetbrains.com/clion/).

Шаг 1:

![Установка пакетов CLion - шаг 1](images/CLion-install-step1.png)

Шаг 2:

![Установка пакетов CLion - шаг 2](images/CLion-install-step2.png)

Шаг 3:

![Установка пакетов CLion - шаг 3](images/CLion-install-step3.png)

Шаг 4:

![Установка пакетов CLion - шаг 4](images/CLion-install-step4.png)

Шаг 5:

![Установка пакетов CLion - шаг 5](images/CLion-install-step5.png)

Шаг 6:

![Установка пакетов CLion - шаг 6](images/CLion-install-step6.png)

Шаг 7:

![Установка пакетов CLion - шаг 7](images/CLion-install-step7.png)

Для работы CLion вам потребуется учетная запись JetBrains. Создайте ее по [ссылке](https://account.jetbrains.com/login).

Чтобы получить бесплатную лицензию для учащихся, необходимо иметь адрес электронной почты, связанный с учебным заведением, и выполнить действия, указанные в [статье](https://www.jetbrains.com/community/education/#students). Если пока не имеете, можно воспользоваться "Free 30-day Trial".
