---
sidebar_position: 1
---

# Разработка в операционных систем в Windows 10 и Windows 11

При работе в операционной системе Windows 10 и Windows 11 на выбор предлагается использовать Visual Studio Code, CLion или Visual Studio для разработки программ на С/C++.

Разработчик программ C/C++ всегда в своем наборе инструментов имеет компилятор и отладчик. В качестве компилятора на Windows можно использовать [GCC](https://gcc.gnu.org/), а для отладки - [GDB](https://www.sourceware.org/gdb/). Получить их можно из наборов инструментов и библиотек такие, как MinGW (простой способ установить, через [MSYS2](https://www.msys2.org/)) и [Cygwin](https://cygwin.com/).

## Visual Studio Code

Информацию по работе с Visual Studio Code можно получить из [документации](https://code.visualstudio.com/docs).

Для начала разработки программ на С++ в Visual Studio Code имеется [серия статей из документации](https://code.visualstudio.com/docs/cpp/). Для Windows предлагается набор инструментов разработчика получить из [MinGW](https://code.visualstudio.com/docs/cpp/config-mingw) или развернуть их в [WSL](https://code.visualstudio.com/docs/cpp/config-wsl).

В данной работе для работы с Visual Studio Code предлагается:

1. Установить MSYS2 (смотреть [здесь](./MSYS2/Install.md)).
2. Установить CMake (смотреть [здесь](./CMake/Install.md)).
3. Установить Visual Studio Code (смотреть [здесь](./VSCode/Install.md)).
4. Установить расширения для Visual Studio Code (смотреть [здесь](../VSCode/Extensions.md)).
5. Создать тестовый проект и выполнить его отладку (смотреть [здесь](./VSCode/CreateAndDebug.md)).

Об отладке в Visual Studio Code подробно рассказано в [статье](https://code.visualstudio.com/docs/editor/debugging).

## CLion

Информацию по работе с CLion можно получить из [документации](https://www.jetbrains.com/help/clion/).

В данной работе для работы с CLion предлагается:

1. Установить Cygwin (смотреть [здесь](./Cygwin/Install.md)).
2. Установить CMake (смотреть [здесь](./CMake/Install.md)).
3. Установить CLion (смотреть [здесь](./CLion/Install.md)).
4. Создать тестовый проект и выполнить его отладку (смотреть [здесь](./CLion/CreateAndDebug.md)).

Об отладке в CLion подробно рассказано в [статье](https://www.jetbrains.com/help/clion/configuring-debugger-options.html).

## Visual Studio

Visual Studio — линейка продуктов компании Microsoft, включающих интегрированную среду разработки программного обеспечения и ряд других инструментов.

Любой индивидуальный разработчик может создавать бесплатные или платные приложения с помощью Visual Studio Community. Visual Studio Community может использовать неограниченное число пользователей в организации в следующих случаях: в учебных аудиториях, для научных исследований или участия в проектах с открытым кодом.

В данной работе для работы с Visual Studio предлагается:

1. Установить CMake (смотреть [здесь](./CMake/Install.md)).
2. Установить Visual Studio Community (смотреть [здесь](./VS/Install.md)).
3. Создать тестовый проект и выполнить его отладку (смотреть [здесь](./VS/CreateAndDebug.md)).

Об отладке в Visual Studio подробно рассказано в [статье](https://docs.microsoft.com/ru-ru/visualstudio/debugger/getting-started-with-the-debugger-cpp?view=vs-2022).

## Решение возможных проблем

Если имеется проблема с работой Visual Studio Code, то рекомендуется попробовать CLion. При этом необязательно удалять MSYS2 (при установке CLion не устанавливать Cygwin). При настройке Toolchains в CLion указать расположение MinGW.

Если имеются проблемы с MSYS2, то можно воспользоваться Cygwin. Перед этим необходимо удалить MSYS2 и добавленную переменную окружения. Также необходимо убедиться, что папка с установкой MSYS2 удалена.

Если имеются проблемы и с Visual Studio Code, и с CLion, то можно обратить внимание на Visual Studio. При установке Visual Studio можно выбрать рабочую нагрузку, в которой будет поставляться собственные инструменты разработки на C++. Поэтому проблем с разворачиванием компилятора и отладчика не возникает.

Если все еще имеются проблемы, то рекомендуется обратить внимание на [WSL](https://code.visualstudio.com/docs/cpp/config-wsl).
