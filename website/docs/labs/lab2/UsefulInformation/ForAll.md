---
sidebar_position: 1
---

# Для всех

## `_CRT_SECURE_NO_WARNINGS` для проекта Visual Studio

При использовании таких функций, как `scanf`, по умолчанию Visual Studio в проектах консольного приложения будет выдавать ошибку, похожую на:

```bash
'scanf': This function or variable may be unsafe. Consider using scanf_s instead. To disable deprecation, use _CRT_SECURE_NO_WARNINGS. See online help for details.
```

Для того, чтобы подавить эту ошибку, необходимо в `Preprocessor Definitions` указать флаг `_CRT_SECURE_NO_WARNINGS`. Эти определения находятся в настройках проекта во вкладке `Configuration Properties>C/C++>Preporocessor`.

В видео продемонстрировано как подавить эту ошибку:

<iframe src="https://vk.com/video_ext.php?oid=-215612996&id=456239025&hash=3e196ec8aba8ef35&hd=2" width="100%" height="480" allow="autoplay; encrypted-media; fullscreen; picture-in-picture;" frameborder="0" allowfullscreen></iframe>
