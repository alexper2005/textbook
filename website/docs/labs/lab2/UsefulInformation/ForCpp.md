---
sidebar_position: 3
---

# Для программ на  C++

## warning: ISO C++ forbids converting a string constant to ‘char*’

При компиляции программы возникает предупреждение:

```bash
warning: ISO C++ forbids converting a string constant to ‘char*’ [-Wwrite-strings]
   time_of_the_day = "дня";
```

Операция прямого присвоения строкового литерала указателю считается разработчиками устаревшей. В данной работе используется C++ 17 версии.

Предупреждение возникает в этой части:

```cpp
char *time_of_the_day;
time_of_the_day = "утра";
```

Требуется использовать явное приведение к типу `char *`:

```cpp
char *time_of_the_day;
time_of_the_day = (char *) "утра";
```

Или использовать `const char *`:

```cpp
const char *time_of_the_day = "утра";
```

Подробнее в статьях:

- [warning: ISO C++ forbids converting a string constant to 'char*' [-Wwrite-strings]](https://russianblogs.com/article/63201013306/)
- [error: ISO C++ forbids converting a string constant to ‘char*’](https://stackoverflow.com/questions/71991901/error-iso-c-forbids-converting-a-string-constant-to-char)
